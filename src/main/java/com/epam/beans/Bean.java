package com.epam.beans;

import com.epam.validator.BeanValidator;

public class Bean implements BeanValidator {
    private String name;
    private int value;

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return name + "{" + value + "}";
    }

    @Override
    public boolean validate() {
        return (value >= 0) && (name != null);
    }
    public void init() {
        System.out.println("initiating: " + this);
    }

    public void destroy() {
        System.out.println("destroying: " + this);
    }
}

