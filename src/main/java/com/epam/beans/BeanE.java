package com.epam.beans;

public class BeanE extends Bean{
    private BeanA beanA;
    public void setBeanA(BeanA beanA) {
        this.beanA = beanA;
    }

    @Override
    public String toString() {
        return "BEAN: " + beanA;
    }
}
