package com.epam.beans;

public class BeanA extends Bean {
    private BeanB beanB;
    private BeanC beanC;
    public void setBeans(BeanB beanB, BeanC beanC) {
        this.beanB = beanB;
        this.beanC = beanC;
    }

    @Override
    public String toString() {
        return "BEANS " + beanB + " and " + beanC;
    }
}
