package com.epam;

import com.epam.beans.*;
import com.epam.config.Config;
import com.epam.config.Config1;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App {
    public static void main(String[] args) {
        try {
            ApplicationContext context =
                    new AnnotationConfigApplicationContext(Config.class);
            BeanB beanB = context.getBean(BeanB.class);
            System.out.println(beanB);
            BeanC beanC = context.getBean(BeanC.class);
            System.out.println(beanC);
            BeanD beanD = context.getBean(BeanD.class);
            System.out.println(beanD);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
