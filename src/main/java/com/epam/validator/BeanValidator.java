package com.epam.validator;

public interface BeanValidator {
    boolean validate();
}
